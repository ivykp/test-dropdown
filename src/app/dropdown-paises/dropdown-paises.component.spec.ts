import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownPaisesComponent } from './dropdown-paises.component';

describe('DropdownPaisesComponent', () => {
  let component: DropdownPaisesComponent;
  let fixture: ComponentFixture<DropdownPaisesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownPaisesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownPaisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
