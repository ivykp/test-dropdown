import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { timer } from 'rxjs/observable/timer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  form: FormGroup;
  pais: any;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.pais = { codigo: 'CO', label: 'Colombia' };
    // timer(3000).subscribe(_ => this.pais = { codigo: 'CO', label: 'Colombia' });
  }
}
