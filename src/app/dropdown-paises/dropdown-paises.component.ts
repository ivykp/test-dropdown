import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import { timer } from 'rxjs/observable/timer';


@Component({
  selector: 'app-dropdown-paises',
  templateUrl: './dropdown-paises.component.html',
  styleUrls: ['./dropdown-paises.component.css']
})
export class DropdownPaisesComponent implements OnInit {
  @Input() form: FormGroup;
  opciones: any[];

  constructor() { }

  ngOnInit() {
    Observable
      .of([{ codigo: 'CO', label: 'Colombia' }, { codigo: 'ES', label: 'España' }])
      .delay(5000)
      .subscribe((data) => {
        this.opciones = data;
        // timer(1).subscribe(_ => this.form.get('pais').setValue(this.defaultValue));
      });
  }

}
