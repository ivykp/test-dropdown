import {
  Component,
  forwardRef,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export const DropdownGeneralComponentValueAccessor: any = {
  provide: NG_VALUE_ACCESSOR,
  // tslint:disable-next-line:no-use-before-declare
  useExisting: forwardRef(() => DropdownComponent),
  multi: true
};

@Component({
  selector: 'app-dropdown',
  templateUrl: 'dropdown.component.html',
  providers: [DropdownGeneralComponentValueAccessor]
})

export class DropdownComponent implements ControlValueAccessor, OnInit, OnChanges {
  @Input() valorPorDefecto: any;
  @Input() llavePropiedadLabel: string;
  @Input() listaOpciones: any[];
  @Input() placeholder = 'Seleccione un elemento';
  @Input() nombreDropdown = 'dropdownGeneral';
  @Input() disabled: Boolean;
  @Output() opcionSeleccionada = new EventEmitter<any>();

  private _value: any;
  valorInicial: any;

  constructor() {
    this.disabled = this.disabled || false;
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('On Changes', changes);
    const hayCambioOpciones = changes.hasOwnProperty('listaOpciones') &&
      !changes['listaOpciones'].isFirstChange() &&
      changes['listaOpciones'].currentValue.length > 0;

    if (hayCambioOpciones) {
      if (this.valorInicial) {
        this.value = this.valorInicial;
        this.valorInicial = undefined;
      } else {
        this.value = undefined; // Reset
      }
    }
  }

  ngOnInit() {
    if (this.valorPorDefecto) {
      this.value = this.valorPorDefecto;
    }
  }

  get value(): any {
    return this._value;
  }

  equals(obj1: Object, obj2: Object): boolean {
    return JSON.stringify(obj1) === JSON.stringify(obj2);
  }

  validarCambioInicial(value: any): void {
    const cambioInicial = value && !this.listaOpciones;
    if (cambioInicial) {
      this.valorInicial = value;
    }
  }

  set value(value: any) {
    this.validarCambioInicial(value);
    const cambioValido = value && this.listaOpciones;
    if (!this.equals(value, this.value) && cambioValido) {
      this.actualizarValorSeleccionado(value);
    }
  }

  actualizarValorSeleccionado(opcSeleccionada: any): void {
    const opcionActual = this.listaOpciones && this.listaOpciones.find(opc => {
      console.log('Comparando: ', opc, opcSeleccionada);
      return this.equals(opc, opcSeleccionada);
    });
    if (opcionActual) {
      console.log('Se va a actualizar el value: ', opcionActual);
      this._value = opcSeleccionada;
      this.opcionSeleccionada.emit(opcionActual);
      this.onChange(this.value);
    }
  }

  // Estos callbacks se deben llamar para notificar hacia afuera estos eventos
  onChange = (_: any) => { /* Empty */ };
  onTouched = (_: any) => { /* Empty */ };

  // Implementacion de la interface ControlValueAccessor
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any): void {
    this.value = value;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
