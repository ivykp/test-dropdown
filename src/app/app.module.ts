import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PinputComponent } from './pinput/pinput.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownComponent } from './dropdown/dropdown.component';
import { DropdownPaisesComponent } from './dropdown-paises/dropdown-paises.component';


@NgModule({
  declarations: [
    AppComponent,
    PinputComponent,
    DropdownComponent,
    DropdownPaisesComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
