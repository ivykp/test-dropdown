import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';

@Component({
  selector: 'app-pinput',
  templateUrl: './pinput.component.html',
  styleUrls: ['./pinput.component.css']
})
export class PinputComponent implements OnInit, OnChanges {
  form: FormGroup;
  @Input() pais: any;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: [''],
      secondName: [''],
      pais: ['', [Validators.required]],
    });

    /* Observable.timer(10).subscribe(_ => this.form.patchValue({
      pais: { codigo: 'ES', label: 'España' },
      name: 'Cristopher'
    }));*/
  }

  ngOnChanges(changes: SimpleChanges) {
    /* console.log('Changes pinput', changes);
    if (changes['pais'] && !changes['pais'].isFirstChange()) {
      console.log('Change pais');
      this.form.get('pais').setValue(changes['pais'].currentValue);
    } */
  }


}
